var webpack = require('webpack');
module.exports = {
  entry: [
  './app/app.jsx',
  'script-loader!jquery/dist/jquery.js'
  ],
  externals:{
    jquery : 'jQuery'
  },
  plugins:[
    new webpack.ProvidePlugin({
      '$': 'jquery',
      'jQuery': 'jquery'
    })
  ],
  output: {
    path: __dirname,
    filename: './public/bundle.js'
  },
  resolve: {
    modules: [__dirname, 'node_modules'],
    alias: {
     PageWrapper : 'app/components/PageWrapper.jsx',
     Header : 'app/components/Header.jsx',
     Main : 'app/components/Main.jsx',
     BlogList : 'app/components/BlogList.jsx',
     AddBlog : 'app/components/AddBlog.jsx',
     Posts : 'app/components/Posts.jsx',
     PostList : 'app/components/PostList.jsx',
     Post : 'app/components/Post.jsx',
     Pagination : 'app/components/Pagination.jsx',
     PostSearchForm : 'app/components/PostSearchForm.jsx',
     BlogForm : 'app/components/BlogForm.jsx',
     EditBlog : 'app/components/EditBlog.jsx',
     BlogFormEdit : 'app/components/BlogFormEdit.jsx',
     DeleteBlog : 'app/components/DeleteBlog.jsx',
     ViewPost : 'app/components/ViewPost.jsx',
     getPostList : 'app/api/getPostList.jsx',
     MianStyle : 'app/scss/style.scss',
    },
    extensions: ['.js', '.jsx']
  },
  module: {
    loaders: [
      {
        loader: 'babel-loader',
        query: {
          presets: ['react', 'latest','stage-0']
        },
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/
      }
    ]
  },
  devtool: 'cheap-module-eval-source-map'
};
