//var React = require('react');
//var ReactDOM = require('react-dom');
import React from 'react'
import ReactDOM from 'react-dom'
//import { Button } from 'react-bootstrap'
//import {PageWrapper} from 'PageWrapper';
var PageWrapper = require('PageWrapper');
import {
  HashRouter as Router,
  Route,
  Link,
  hashHistory
} from 'react-router-dom'
require('style-loader!css-loader!sass-loader!MianStyle');
const BasicExample = () => (
  <Router history={hashHistory}>
  	<Route>
  		<PageWrapper/>
  	</Route>
  </Router>
)


ReactDOM.render(
  <BasicExample/>,
  document.getElementById('app')
);
