//import axios from 'axios'
var axios = require('axios');

//const REQUEST_URL="http://localhost/myblog/api/blog.php";
const REQUEST_URL="https://frndzit.com/myblog/api/blog.php";

module.exports ={
	getSinglePost: function(postno){
		var request_url=`${REQUEST_URL}`;
		return axios.get(request_url, {
		    params: {
		      postno: postno
		    }
		  })
		  .then(function (res) {
		    if(res.status!==200){
		    	
		    
		    	throw new Error(res.statusText);
		    }else{
		    	return res.data;
		    }
		  })
		  .catch(function (error) {
		    throw new Error(error.statusText)
		  });


		 

	},
	getPost: function(pageno,perPage,search){
		var request_url=`${REQUEST_URL}`;
		return axios.get(request_url, {
		    params: {
		      pageno: pageno,
		      perPage: perPage,
		      search: search,
		    }
		  })
		  .then(function (res) {
		    if(res.status!==200){
		    	
		    
		    	throw new Error(res.statusText);
		    }else{
		    	return res.data;
		    }
		  })
		  .catch(function (error) {
		    throw new Error(error.statusText)
		  });


		 

	},

	addPost :function(title,detail,startDate,author){
		var request_url=`${REQUEST_URL}`;
		return axios.post(request_url,{
		    title: title,
		    detail: detail,
		    created: startDate,
		    author: author,
		  })
		  .then(function (res) {
		    if(res.status!==200){
		    	
		    
		    	throw new Error(res.statusText);
		    }else{
		    	return res.data;
		    }
		  })
		  .catch(function (error) {
		    throw new Error(error.statusText)
		  });

	},
	DeletePost :function(id){
		var request_url=`${REQUEST_URL}`;
		return axios.post(request_url,{
		    delteid: id
		  })
		  .then(function (res) {
		    if(res.status!==200){
		    	throw new Error(res.statusText);
		    }else{
		    	return res.data;
		    }
		  })
		  .catch(function (error) {
		    throw new Error(error.statusText)
		  });

	},

	editPost :function(title,detail,startDate,author,id){
		var request_url=`${REQUEST_URL}`;
		return axios.put(request_url,{
		    title: title,
		    detail: detail,
		    created: startDate,
		    author: author,
		    id: id
		  })
		  .then(function (res) {
		    if(res.status!==200){
		    	
		    
		    	throw new Error(res.statusText);
		    }else{
		    	return res.data;
		    }
		  })
		  .catch(function (error) {
		    throw new Error(error.statusText)
		  });

	}
}