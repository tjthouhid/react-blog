 
import React from 'react'
import {FaSearch} from 'react-icons/lib/fa'
import {
  HashRouter as Router,
  Route,
  Link
} from 'react-router-dom'

class PostSearchForm extends React.Component {

  constructor(props) {
    super(props);
    this.submitForm = this.submitForm.bind(this);
  }

  submitForm(e){
    e.preventDefault();
    var search=this.refs.search.value;
    
      this.props.onSearch(search)
   
  }
  

  render() {
   // var {pageno}=this.state;
    return (
          <div className="row">
            <div className="col-md-4">      
              <form className="form-inline" onSubmit={this.submitForm}>
                <div className="form-group col-md-10">    
                  <div className="input-group search-box">
                    <input type="text" className="form-control" placeholder="Search your post.." ref="search" aria-describedby="basic-addon2"/>
                    
                  </div>
                </div>
                <button type="submit" className="btn btn-primary col-md-2"><FaSearch/></button>
              </form>       
            </div>
          </div>
              
                
                
               

            
    );
  }
}


module.exports=PostSearchForm;
