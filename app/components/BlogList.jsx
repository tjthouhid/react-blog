 
import React from 'react'
import {FaSearch} from 'react-icons/lib/fa'
var Posts = require('Posts');
import {
  HashRouter as Router,
  Route,
  Link
} from 'react-router-dom'
class BlogList extends React.Component {

  

  render() {
   // var {pageno}=this.state;
   //console.log(this.props.match)
    var pageno=this.props.match.params.pageno;
    return (
      <div className="container">
            <h2 className="title-text text-center">Item list {pageno}</h2>
            <div className="list-box">
               
                
                 <Posts pageNo={pageno} perPage={3}/>
               

            </div>
        </div>
    );
  }
}


module.exports=BlogList;
