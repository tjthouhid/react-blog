 
import React from 'react'
import {FaSearch} from 'react-icons/lib/fa'
import {
  HashRouter as Router,
  Route,
  NavLink
} from 'react-router-dom'



class Pagination extends React.Component {

	constructor(props) {
		   super(props);

		   
		 }


  render() {
  	var {totalPage}=this.props;

   
  	var renderpagination = (totalPage) => {
  	  if(totalPage>0){
  	       var indents=[],i;
           for(i=1;i<=totalPage;i++){
          var url="/posts";
            
              indents.push(<li key={i}><NavLink exact={true} to={`${url}/${i}`} activeClassName="active">{i}</NavLink></li>);
             
           }
  	      return(
  	        <ul className="pagination">
                {indents}              
               
           </ul>
  	        );
  	    
  	  }else{
  	   
  	  }
  	 
  	};
  	
  
		 
    return (

        <div aria-label="Page navigation" className="my-pagination">
           
            {renderpagination(totalPage)}
        </div>
    		
    	  	
    	  
    	
    );
  }
}

module.exports=Pagination;