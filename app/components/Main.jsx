
import React from 'react'
var BlogList = require('BlogList');
var AddBlog = require('AddBlog');
var EditBlog = require('EditBlog');
var ViewPost = require('ViewPost');
var DeleteBlog = require('DeleteBlog');
import {
  HashRouter as Router,
  Route,
  Link
} from 'react-router-dom'
const Main =({match})=> (
	<div className="main-body">
		<Route exact={true} path="/" component={BlogList}/>
		<Route exact={true} path="/posts/" component={BlogList}/>
		<Route exact={true} path="/posts/:pageno" component={BlogList}/>
		<Route exact={true} path="/view/:postno" component={ViewPost}/>
		
		<Route exact={true} path="/addblog" component={AddBlog}/>
		<Route exact={true} path="/edit/:postno" component={EditBlog}/>
		<Route exact={true} path="/delete/:postno" component={DeleteBlog}/>
	    
	</div>
);

module.exports=Main;