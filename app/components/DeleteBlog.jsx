 
import React from 'react'
import { Alert } from 'react-bootstrap';
var getPostList = require('getPostList');

import {
  HashRouter as Router,
  Route,
  Link,
  NavLink
} from 'react-router-dom'


class DeleteBlog extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
          id:props.match.params.postno,
          deleted:false
        };
    this.postDeleteForm = this.postDeleteForm.bind(this);
  }


  postDeleteForm(e){
    e.preventDefault();
    var id=this.state.id;
    var that=this;
    //console.log(id)
    getPostList.DeletePost(id).then(function(data){
         that.setState({
               deleted: true
             });

        },function(errormsg){

        });
    
    
    
   
  }
  
  
  


  render() {
    var {id}=this.state;
    var postno=this.props.match.params.postno;

    var renderposView= () => {
      if(this.state.deleted===false){
        return(

          <div className="view-box text-center">
          <h1 className="text-center">Are You Sure?</h1>
            <form onSubmit={this.postDeleteForm}> 
              <button type="submit" className="btn-style-2">Delete</button>&nbsp;&nbsp;&nbsp;
              <NavLink to="/" className="btn-style-1">Back</NavLink>
            </form>
               
              
           </div>
                     
          );

      }else {
        return(

           <div className="alert-show">
              <Alert bsStyle="success">
              <strong>Congrates!</strong> Your Post Deleted Successfully.Go Back to<NavLink exact={true} to="/">List View</NavLink>
            </Alert>
           </div>
                     
          );

      }
      
    };

  
    return (
     <div className="container">
           <h2 className="title-text text-center">Delete Page</h2>
          {renderposView()}
       </div>
    );
  }
}





module.exports=DeleteBlog;
