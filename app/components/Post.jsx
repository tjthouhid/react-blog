 
import React from 'react'
import {FaCog,FaTrashO} from 'react-icons/lib/fa'
import {
  HashRouter as Router,
  Route,
  Link,
  NavLink
} from 'react-router-dom'

class PostList extends React.Component {

	

  render() {
    var {title,detail,author,id,created}=this.props;    
    return (
    	    <li>
    	        
    	        <div className="item-info">
    	            <h2 className="subtitle-text item-name">{title}</h2>
    	            <p className="p-text item-dtls">{detail}</p> 
                  <p className="list-footer"><strong>Author :</strong><span>{author}</span> </p>
    	            <p className="list-footer date"><strong>Date :</strong><span>{created}</span> </p>
              </div>
    	        <div className="item-option">
                  <NavLink to= {`/view/${id}`}  className="icons-option3">View</NavLink>
    	            <NavLink  to={`/edit/${id}`} className="icons-option">Edit <FaCog/></NavLink>
    	            <NavLink to={`/delete/${id}`} className="icons-option2">Delete <FaTrashO/></NavLink>
    	        </div>
    	    </li>
    	  
    	  
    	
    );
  }
}

module.exports=PostList;