 
import React from 'react'
import ReactDOM from 'react-dom'
import DatePicker from 'react-datepicker'
import Moment from 'moment'
import 'style-loader!css-loader!react-datepicker/dist/react-datepicker.css';
import {FaCloudUpload} from 'react-icons/lib/fa'


import {
  HashRouter as Router,
  Route,
  Link,
  NavLink
} from 'react-router-dom'
 
class BlogForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
          startDate: Moment(),
          detail: "",
          title: "",
          author: ""
        };
   
    this.postSubmitForm = this.postSubmitForm.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleDetail = this.handleDetail.bind(this);
    this.handleTitle = this.handleTitle.bind(this);
    this.handleAuthor = this.handleAuthor.bind(this);
  }

  handleChange(date) {
      this.setState({
        startDate: date
      });
    }
  handleDetail(event) {
      this.setState({
        detail: event.target.value
      });
    }
  handleTitle(event) {
      this.setState({
        title: event.target.value
      });
    }
  handleAuthor(event) {
      this.setState({
        author: event.target.value
      });
    }

  postSubmitForm(e){
    e.preventDefault();
    var title=this.refs.title.value;
    var detail=this.state.detail;
    var startDate=this.state.startDate;
    var author=this.state.author;
    
    this.props.onaddPost(title,detail,startDate,author)    
     // this.props.onSearch(search);
      // var d="ok";
      // getPostList.addPost(title,detail,d).then(function(data){
      //  console.log(data);
      //  //console.log(data)

      // },function(errormsg){

      // });
   
  }

  render() {
  
    return (
      <form onSubmit={this.postSubmitForm}>
        <div className="all-inputs">
            <input type="text" className="name" ref="author" onChange={this.handleAuthor} value={this.state.author} placeholder="Author Name" />
           
            <input type="text" className="name" ref="title" onChange={this.handleTitle} value={this.state.title} placeholder="Title" />
            
            <textarea className="item-dtls" ref="detail" onChange={this.handleDetail} value={this.state.detail} placeholder="Write Detail..."></textarea>
            <DatePicker
                todayButton={"Today"}
                selected={this.state.startDate}
                onChange={this.handleChange}
              
            />
            
          
            <button type="submit" className="btn-style-1">Save</button>
             <NavLink to="/" className="btn-style-2">Discard</NavLink>
            
        </div>
      </form>
    );
  }
}


module.exports=BlogForm;
