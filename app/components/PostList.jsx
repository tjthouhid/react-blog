 
import React from 'react'
import {FaSearch} from 'react-icons/lib/fa'
import {
  HashRouter as Router,
  Route,
  Link
} from 'react-router-dom'
var Post = require('Post');

class PostList extends React.Component {

	

  render() {
    var {posts}=this.props;
    var renderposts = () => {
      return posts.map((post)=>{
        return(
          <Post key={post.id}{...post}/>
          );
      });
    };
    return (
      <ul className="p-none all-list">      
      {renderposts()}  	  
    	</ul>
    );
  }
}

module.exports=PostList;