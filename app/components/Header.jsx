import React from 'react'
import {
  HashRouter as Router,
  Route,
  NavLink,
  Link
} from 'react-router-dom'
//{match.path === '/posts' || '/' ? 'active' : 'notActive'}
class Header extends React.Component {
  
  

  render() {
   // var {pageno}=this.state;
   
    var match=this.props.match;
    var url="not";
    var cls="active";
    if(match){
    	url=match.path;
    }
    switch(url) {
    case '/posts':
        cls="active";
        break;
    case '/':
        cls="active";
        break;
    case '/posts/:pageno':
        cls="active";
    default:
        cls="";
        break;
    }

    return (
      <header>
          <nav className="navbar navbar-default">
              <div className="container">
                  

                  
                      <ul className="nav navbar-nav">
                          <li><NavLink exact={true} to="/" className={cls}>List View</NavLink></li>
                          <li><NavLink to="/addblog" activeClassName="active">Add Blog</NavLink></li>
                                        
                      </ul>
                  
              </div>
          </nav>
      </header>
    );
  }
}


module.exports=Header;