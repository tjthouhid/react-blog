import React from 'react'
var Header = require('Header');
var Main = require('Main');
import {
  HashRouter as Router,
  Route,
  Link
} from 'react-router-dom'
const PageWrapper =()=> (
   <div className="page-wrapper">
   	<Header/>
   	<Main/>
    
   </div>
 );

 module.exports=PageWrapper;