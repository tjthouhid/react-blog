 
import React from 'react'
import { Alert } from 'react-bootstrap';
var getPostList = require('getPostList');

import {
  HashRouter as Router,
  Route,
  Link,
  NavLink
} from 'react-router-dom'
var BlogForm = require('BlogForm');

class ViewPost extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
          title:"Loding....",
          detail:false,
          created :"",
          author :"",
          id:props.match.params.postno
        };
   // this.handlePost = this.handlePost.bind(this);
  }

  
  componentDidMount() {
    var that=this;
    var postno=this.props.match.params.postno;
   // console.log(postno);
    //var search=that.state.search;
    getPostList.getSinglePost(postno).then(function(data){
     // console.log(data)
      if(data.getPost==="yes"){
        that.setState({ 
          title :data.posts.title,
          detail :data.posts.detail,
          created :data.posts.created,
          author :data.posts.author,
          id:data.posts.id
        });
      }else{
        that.setState({ 
          title :"No Data Found",
          detail :"",
          created :"",
          author :"",
          id:postno
        });

      }
      
      //console.log(data)

    },function(errormsg){

    });
    
     
   } 
  


  render() {
    var {title,detail,created,author,id}=this.state;
    var postno=this.props.match.params.postno;

    var renderposView= () => {
      if(this.state.detail===false){
        return(

          <div className="view-box">
               
               <h2 className="subtitle-text item-name">{title}</h2>   

               <NavLink to="/" className="btn-style-1">Back</NavLink>
           </div>
                     
          );

      }else if(this.state.detail===""){
        return(

          <div className="view-box">
               
               <h2 className="subtitle-text item-name">No view Page Availabe.</h2>
              

               <NavLink to="/" className="btn-style-1">Back</NavLink>
           </div>
                     
          );

      }else{
        return(

          <div className="view-box">
               
               <h2 className="subtitle-text item-name">{title}</h2>
               <p className="p-text item-dtls">{detail} </p>
               <p className="list-footer"><strong>Author :</strong><span>{author}</span> </p>
              <p className="list-footer date"><strong>Date :</strong><span>{created}</span> </p>

               <NavLink to={`/edit/${id}`} className="btn-style-1">Edit</NavLink>&nbsp;&nbsp;&nbsp;<NavLink to="/" className="btn-style-2">Back</NavLink>
           </div>
                     
          );

      }
      
    };

  
    return (
     <div className="container">
           <h2 className="title-text text-center">View Page</h2>
          {renderposView()}
       </div>
    );
  }
}





module.exports=ViewPost;
