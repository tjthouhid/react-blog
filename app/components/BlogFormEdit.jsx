 
import React from 'react'
import ReactDOM from 'react-dom'
import DatePicker from 'react-datepicker'
import Moment from 'moment'
import 'style-loader!css-loader!react-datepicker/dist/react-datepicker.css';
import {FaCloudUpload} from 'react-icons/lib/fa'


import {
  HashRouter as Router,
  Route,
  Link,
  NavLink
} from 'react-router-dom'

var getPostList = require('getPostList');
 
class BlogFormEdit extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
          startDate: "",
          detail: "",
          title: "Please wait....",
          author: "Please wait....",
          id:props.postno
        };
   
    this.postSubmitForm = this.postSubmitForm.bind(this);
    this.handleDate = this.handleDate.bind(this);
    this.handleDetail = this.handleDetail.bind(this);
    this.handleTitle = this.handleTitle.bind(this);
    this.handleAuthor = this.handleAuthor.bind(this);
  }

  handleDate(date) {
      this.setState({
        startDate: date
      });
    }
  handleDetail(event) {
      this.setState({
        detail: event.target.value
      });
    }
  handleTitle(event) {
      this.setState({
        title: event.target.value
      });
    }
  handleAuthor(event) {
      this.setState({
        author: event.target.value
      });
    }


    componentDidMount() {
      var that=this;
      var postno=this.state.id;
    //console.log("post no",this.state);
      //var search=that.state.search;
      getPostList.getSinglePost(postno).then(function(data){
        //console.log(Moment(,"M/D/Y"))
     
        // console.log(datess)
         //console.log(Moment())
        if(data.getPost==="yes"){
          var datess=Moment(data.posts.created);
          that.setState({ 
            startDate :datess,
            detail :data.posts.detail,
            title :data.posts.title,
            author :data.posts.author,
            id:data.posts.id
          });
        }else{
          that.setState({ 
            startDate :Moment(),
            detail :"",
            title :"No Data Found",
            author :"",
            id:postno
          });

        }
        
        //console.log(data)

      },function(errormsg){

      });
      
       
     } 

  postSubmitForm(e){
    e.preventDefault();
    var title=this.refs.title.value;
    var detail=this.state.detail;
    var startDate=this.state.startDate;
    var author=this.state.author;
    var id=this.state.id;
    
    this.props.onaddPost(title,detail,startDate,author,id);    
     // this.props.onSearch(search);
      // var d="ok";
      // getPostList.addPost(title,detail,d).then(function(data){
      //  console.log(data);
      //  //console.log(data)

      // },function(errormsg){

      // });
   
  }

  render() {

    return (
      <form onSubmit={this.postSubmitForm}>
        <div className="all-inputs">
            <input type="text" className="name" ref="author" onChange={this.handleAuthor} value={this.state.author} placeholder="Author Name" />
           
            <input type="text" className="name" ref="title" onChange={this.handleTitle} value={this.state.title} placeholder="Title" />
            
            <textarea className="item-dtls" ref="detail" onChange={this.handleDetail} value={this.state.detail} placeholder="Write Detail..."></textarea>
            <DatePicker
                todayButton={"Today"}
                selected={this.state.startDate}
                onChange={this.handleDate}
              
            />
            
          
            <button type="submit" className="btn-style-1">Save</button>
            <NavLink to="/" className="btn-style-2">Discard</NavLink>
            
        </div>
      </form>
    );
  }
}


module.exports=BlogFormEdit;
