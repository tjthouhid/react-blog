 
import React from 'react'
import { Alert } from 'react-bootstrap';
var getPostList = require('getPostList');

import {
  HashRouter as Router,
  Route,
  Link,
  NavLink
} from 'react-router-dom'
var BlogForm = require('BlogForm');

class AddBlog extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
          error_form:false,
          success_form:false
        };
    this.handlePost = this.handlePost.bind(this);
  }

  

  handlePost(title,detail,startDate,author){
   if(title.length<=0 ||detail.length<=0 ||startDate.length<=0 ||author.length<=0 ){
      this.setState({ 
        error_form :true
      });
   }else{
      var that=this;
    getPostList.addPost(title,detail,startDate,author).then(function(data){
     that.setState({ 
         error_form :false,
         success_form:true
       });

    },function(errormsg){

    });
    

   }
    
    
   
  }

  render() {
    var {error_form,success_form}=this.state;
    var renderpostForm = () => {
      if(error_form===false && success_form===false){
          return(
            <div>
              <h2 className="title-text text-center">Add Post</h2>
              
              <BlogForm onaddPost={this.handlePost}/>
            </div>
            );
        }else if(error_form===true){
          return(
              
              <div>
                <h2 className="title-text text-center">Add Post</h2>
                <div className="alert-show">
                    <Alert bsStyle="danger">
                    <strong>Please Fill !</strong> All feilds.
                  </Alert>
                 </div>
                <BlogForm onaddPost={this.handlePost}/>
              </div>

            );
        }else{
          return(
              
               <div className="alert-show">
                 <Alert bsStyle="success">
                 <strong>Congrates!</strong> Your Post Inserted Successfully.Go Back to<NavLink exact={true} to="/">List View</NavLink>
               </Alert>
              </div>

            );
          
        }
    
     
    };

  
    return (
     <div className="container">
         {renderpostForm()}
        
         
     </div>
    );
  }
}





module.exports=AddBlog;
