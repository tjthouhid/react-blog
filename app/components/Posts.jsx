 
import React from 'react'
import {FaSearch} from 'react-icons/lib/fa'
import {
  HashRouter as Router,
  Route,
  Link
} from 'react-router-dom'

var PostList = require('PostList');
var getPostList = require('getPostList');
var Pagination = require('Pagination');
var PostSearchForm = require('PostSearchForm');

class Posts extends React.Component {

	constructor(props) {
		   super(props);

		   this.state = {		    
		     posts :[],
		     totalPosts:0,
		     fetchingPost:false,
		     pageno:props.pageno,
		     search:""
		     	

		     
		   };
		   this.handleSearch = this.handleSearch.bind(this);
		 }

	componentDidMount() {
		var that=this;
		var search=that.state.search;
		this.updatePosts(this.props,that,search);
		
	   
	 } 

	 componentWillReceiveProps(nextProps) {
	     if(this.props.pageNo !== nextProps.pageNo)
	     {
	     	var that=this;
	     	var search=that.state.search;
	     	this.updatePosts(nextProps,that,search);
	          
	     }
	 } 


	 handleSearch(search){
	 	this.setState({ 
	 		search :search
	 	});
	 	var that=this;
	 	//console.log(that.state.search)
	 	this.updatePosts(this.props,that,search);
	 	

	 }


	 updatePosts(uprops,that,search){
	 
	 	var pageno=uprops.pageNo;
	 	var perPage=uprops.perPage;
	 	var search=search;
	 	//console.log(search)
	 	if(!pageno){
	 		pageno=1;
	 	}
	 	getPostList.getPost(pageno,perPage,search).then(function(data){
	 		that.setState({ 
	 			posts :data.posts,
	 			totalPosts :data.totalposts,
	 			fetchingPost:true,
	 			pageno:pageno
	 		});
	 		//console.log(data)

	 	},function(errormsg){

	 	});
	 }

  render() {
  	
  	var {posts,totalPosts,fetchingPost,pageno}=this.state;
  	var {pageno,perPage}=this.props;
  	//console.log(this.props.pageno)
  	var renderpostslist = (posts,totalPosts,fetchingPost,perPage) => {
  		if(fetchingPost===true){
  			  if(posts.length>0){
  			  		var perpage=perPage;
  					var totalPage=Math.ceil(totalPosts/perpage);
  			      return(
  			      	<div>
  			      	
  			        <PostList posts={posts}/>
  			        <Pagination totalPage={totalPage}/> 
  			        </div>
  			        );
  			    
  			  }else{
  			    return(
  			      <h2>No Post Found!</h2>
  			      );
  			  }
  			}else{
  				return(
  				  <h2>Fetching Blog Posts.....!</h2>
  				  );
  			}
  	
  	 
  	};
  	
  
		 
    return (
    		<div>
    		<PostSearchForm onSearch={this.handleSearch}/>
    		<div className="clearfix"></div>
    	    {renderpostslist(posts,totalPosts,fetchingPost,perPage)}
    	    </div>
    	  	
    	  
    	
    );
  }
}

module.exports=Posts;